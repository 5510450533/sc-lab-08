package model1;


public class Main {

	public static void main(String[] args) {
		
	Measurable[] person = new Measurable[3];
	person[0] = new Person("Sucha", 160);
	person[1] = new Person("Kanika", 155);
	person[2] = new Person("Somchai", 178);
	
	double averageHeight = Data.getAverage(person);
	System.out.println("Average Height : " + averageHeight);
	

	Person sucha = new Person("Sucha", 160);
	Person kanika = new Person("Kanika", 152);
	Person somchai = new Person("Somchai", 178);
		
	Measurable minheight = Data.Min(sucha, kanika, somchai);
	Person per = (Person) minheight;
	double height = per.getHeight();
	System.out.println("Minimum Height : " + height);
	
	Country aus = new Country("Australia", 7000000);
	Country chi = new Country("China", 9000000);
	Country thai = new Country("Thailand", 500000);
	
	Measurable mincountry = Data.Min(aus, chi, thai);
	Country count = (Country) mincountry;
	double minarea = count.getArea();
	System.out.println("Minimum Area : " + minarea);
	
	BankAccount labank = new BankAccount("Lalana", 7200);
	BankAccount pibank = new BankAccount("Piyawan", 3000);
	BankAccount mubank = new BankAccount("Munin", 2500);
	
	Measurable minbank = Data.Min(labank, pibank, mubank);
	BankAccount bank = (BankAccount) minbank;
	double minBalance = bank.getBalance();
	System.out.println("Minimum Balance : " + minBalance);
	
	
	}
}
