package model2;

public class Product implements Taxable{
	private String nameProduct;
	private double price;
	
	public Product(String aNamePro, double aPrice){
		nameProduct = aNamePro;
		price = aPrice;
	}
	
	public String getName(){
		return nameProduct;
	}
	
	public double getPrice(){
		return price;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return price*0.07;
	}
}
